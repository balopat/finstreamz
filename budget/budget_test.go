package budget

import (
	// "fmt"
	. "gopkg.in/check.v1"
	"time"
)

func (s *MySuite) TestEmptyBudget(c *C) {
	time := DateFor(2015, time.April, 10)

	c.Assert(s.budget.GetStatement(time), DeepEquals,
		Statement{
			Date: time,
			Balances: map[string]Money{
				cash:    Money{0, USD},
				savings: Money{0, USD},
				debt:    Money{0, USD}}})
}

func (s *MySuite) TestBudgetInventory(c *C) {	
	InventoryDate := DateFor(2015, time.April, 10)
	afterInventoryDate := DateFor(2015, time.April, 12)
	beforeInventoryDate := DateFor(2015, time.April, 9)

	s.budget.ApplyInventory(Inventory{
		Date: InventoryDate,
		Balances: map[string]Money{
			cash:    Money{3.4, USD},
			savings: Money{1424224, USD},
			debt:    Money{100, USD}}})
	
	c.Assert(s.budget.GetStatement(beforeInventoryDate), DeepEquals,
		Statement{
			Date: beforeInventoryDate,
			Balances: map[string]Money{
				cash:    Money{0, USD},
				savings: Money{0, USD},
				debt:    Money{0, USD}}})

	

	c.Assert(s.budget.GetStatement(afterInventoryDate), DeepEquals,
		Statement{
			Date: afterInventoryDate,
			Balances: map[string]Money{
				cash:    Money{3.4, USD},
				savings: Money{1424224, USD},
				debt:    Money{100, USD}}})

	c.Assert(s.budget.GetStatement(InventoryDate), DeepEquals,
		Statement{
			Date: InventoryDate,
			Balances: map[string]Money{
				cash:    Money{3.4, USD},
				savings: Money{1424224, USD},
				debt:    Money{100, USD}}})
}

func (s *MySuite) TestBudgetInventorySorting(c *C) {
	InventoryDate := DateFor(2015, time.April, 8)
	secondInventoryDate := DateFor(2015, time.April, 10)
	betweenInventoriesDate := DateFor(2015, time.April, 9)
	afterSecondInventoryDate := DateFor(2015, time.April, 11)

	s.budget.ApplyInventory(Inventory{
		Date: secondInventoryDate,
		Balances: map[string]Money{

			cash:    Money{1, USD},
			savings: Money{2, USD},
			debt:    Money{3, USD}}})

	s.budget.ApplyInventory(Inventory{
		Date: InventoryDate,
		Balances: map[string]Money{

			cash:    Money{3.4, USD},
			savings: Money{1424224, USD},
			debt:    Money{100, USD}}})

	c.Assert(s.budget.GetStatement(betweenInventoriesDate), DeepEquals,
		Statement{
			Date: betweenInventoriesDate,
			Balances: map[string]Money{

				cash:    Money{3.4, USD},
				savings: Money{1424224, USD},
				debt:    Money{100, USD}}})

	c.Assert(s.budget.GetStatement(afterSecondInventoryDate), DeepEquals,
		Statement{
			Date: afterSecondInventoryDate,
			Balances: map[string]Money{
				cash:    Money{1, USD},
				savings: Money{2, USD},
				debt:    Money{3, USD}}})
}

func (s *MySuite) TestOneTimeCash(c *C) {
	var budget Budget

	time := DateFor(2015, time.April, 10)

	budget.ApplyDeposit(Deposit{time, "cash", Money{1000, USD}, "just some Money"})

	c.Assert(budget.calculateInventoryToDate(zeroInventory(time), time), DeepEquals,
		Inventory{
			Date: time,
			Balances: map[string]Money{
				cash:    Money{1000, USD},
				savings: Money{0, USD},
				debt:    Money{0, USD}}})

	c.Assert(budget.GetStatement(time), DeepEquals,
		Statement{
			Date: time,
			Balances: map[string]Money{
				cash:    Money{1000, USD},
				savings: Money{0, USD},
				debt:    Money{0, USD}}})
}



func (s *MySuite) TestOneTimeCashRangeStatements(c *C) {
	var budget Budget

	t := DateFor(2015, time.April, 10)
	from := DateFor(2015, time.April, 9)
	to := DateFor(2015, time.April, 11)

	budget.ApplyDeposit(Deposit{t, "cash", Money{1000, USD}, "just some Money"})

	

	c.Assert(budget.GetStatementsBetween(from, to), DeepEquals,
		[]Statement{
			Statement{
				Date: from,
				Balances: map[string]Money{
					cash:    Money{0, USD},
					savings: Money{0, USD},
					debt:    Money{0, USD}}},
			Statement{
				Date: t,
				Balances: map[string]Money{
					cash:    Money{1000, USD},
					savings: Money{0, USD},
					debt:    Money{0, USD}}},
			Statement{
				Date: to,
				Balances: map[string]Money{
					cash:    Money{1000, USD},
					savings: Money{0, USD},
					debt:    Money{0, USD}}}})

}
