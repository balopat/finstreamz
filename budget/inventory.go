package budget

import (
	"time"
)

type Inventory struct {
	Date     time.Time
	Balances map[string]Money
}

type byInventoryDate []Inventory

func (s byInventoryDate) Len() int {
	return len(s)
}
func (s byInventoryDate) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s byInventoryDate) Less(i, j int) bool {
	return s[i].Date.Before(s[j].Date)
}

func (i Inventory) AddDeposit(d Deposit) Inventory {
	newInventory := i
	i.Balances[d.Account] = i.Balances[d.Account].Add(d.Amount, d.Date)
	i.Date = d.Date
	return newInventory
}

func (i Inventory) toStatement() Statement {
	return Statement{
		i.Date,
		i.Balances}
}
func zeroInventory(date time.Time) Inventory {
	return Inventory{
		Date: date,
		Balances: map[string]Money{
			cash:    Money{0, USD},
			savings: Money{0, USD},
			debt:    Money{0, USD}}}
}
