package budget

import (
	. "gopkg.in/check.v1"
	"testing"
)

func Test(t *testing.T) { TestingT(t) }

type MySuite struct{
	budget Budget
}

func (s *MySuite) SetUpTest(c *C) {
	s.budget.ResetBudget()
}

var _ = Suite(&MySuite{})