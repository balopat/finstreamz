package budget

import "time"

type Deposit struct {
	Date    time.Time
	Account string
	Amount  Money
	Comment string
}

type byDepositDate []Deposit

func (s byDepositDate) Len() int {
	return len(s)
}
func (s byDepositDate) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s byDepositDate) Less(i, j int) bool {
	return s[i].Date.Before(s[j].Date)
}
