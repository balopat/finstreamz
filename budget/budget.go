package budget

import (
	"sort"
	"time"
	 "github.com/twinj/uuid"
)

const (
	cash    = "cash"
	savings = "savings"
	debt    = "debt"
)

type Budget struct {
	Id uuid.UUID
	Title string
	Inventories []Inventory
	Deposits []Deposit	
}


func (b *Budget) ResetBudget() {
	b.Inventories = []Inventory{}
	b.Deposits = []Deposit{}
}



func (b *Budget) ApplyInventory(Inventory Inventory) {
	b.Inventories = append(b.Inventories, Inventory)	
	sort.Sort(byInventoryDate(b.Inventories))

}

func (b *Budget)  ApplyDeposit(Deposit Deposit) {
	b.Deposits = append(b.Deposits, Deposit)
	sort.Sort(byDepositDate(b.Deposits))
}

func (b Budget) GetStatementsBetween(from time.Time, to time.Time) []Statement {
		statements := []Statement{}

		diff := to.Sub(from)/time.Hour/24

		for day := 0; day <= int(diff); day ++ {
			date := from.Add(time.Duration(day)*time.Hour*24)
			statement := b.GetStatement(date)
			statements = append(statements, statement)
		}
		return statements
}

func (b Budget) GetStatement(date time.Time) Statement {
	lastInventory := b.findInventoryInEffectFor(date)
	finalInventory := b.calculateInventoryToDate(lastInventory, date)
	Statement := finalInventory.toStatement()
	Statement.Date = date
	return Statement
}




func (b Budget) findInventoryInEffectFor(date time.Time) Inventory {
	prevInventory := zeroInventory(DateFor(1900, time.January, 01))

	for _, inv := range b.Inventories {
		if inv.Date.After(date) {
			return prevInventory
		} else {
			prevInventory = inv
		}
	}

	return prevInventory
}

func (b Budget) calculateInventoryToDate(lastInventory Inventory, toDate time.Time) Inventory {
	inventory := lastInventory
	for _, dep := range b.Deposits {
		if (dep.Date.Equal(lastInventory.Date) || dep.Date.After(lastInventory.Date)) &&
			(dep.Date.Before(toDate) || dep.Date.Equal(toDate)) {
			inventory = inventory.AddDeposit(dep)
		}
	}

	inventory.Date = toDate

	return inventory
}

