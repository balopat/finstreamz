package budget

import (	
	. "gopkg.in/check.v1"
)


func (s *MySuite) TestListAllBudgets(c *C) {
	c.Assert(len(ListAllBudgets()), Equals, 2)
}