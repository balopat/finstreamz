package budget

import (
	"fmt"
	"time"
)

type Statement struct {
	Date     time.Time
	Balances map[string]Money
}

func (s Statement) String() string {
	balancesToString := "Balances: \n"
	for account, balance := range s.Balances {
		balancesToString += fmt.Sprintf("\t %s: %s\n", account, balance)
	}
	return fmt.Sprintf("Statement: \nDate: %s \n%s", s.Date, balancesToString)
}
