package budget

import (
 "github.com/twinj/uuid"
)

var uuid1 = uuid.NewV4()
var uuid2 = uuid.NewV4()

var budgets = map[uuid.UUID]Budget{
	uuid1: Budget{
			Id: uuid1,
			Title: "sample plan",
			Inventories: []Inventory{},
			Deposits: []Deposit{},
		},
	uuid2: Budget{
		Id: uuid2,
		Title: "other fantastic sample plan",
		Inventories: []Inventory{},
		Deposits: []Deposit{},
	},
}

func ListAllBudgets() []Budget {
	var returnBudgets []Budget
	for _, budget := range budgets {
	    returnBudgets = append(returnBudgets, budget)
	}
	return returnBudgets
}
