package budget

import (
	"fmt"
	"time"
)

type Money struct {
	Notional float64
	Currency string
}

func (m Money) String() string {
	return fmt.Sprintf("%.2f %s", m.Notional, m.Currency)
}

func (this Money) Add(that Money, date time.Time) Money {
	addedNotional := that.Notional
	if this.Currency != that.Currency {
		exchangeRate := ExchangeRate(this.Currency, that.Currency, date)
		addedNotional = that.Notional * exchangeRate
	}
	return Money{
		Notional: addedNotional + this.Notional,
		Currency: this.Currency}
}
