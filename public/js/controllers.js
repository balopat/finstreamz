 function randomFutureMonths() {
     var months = [];
     var i = 0; 
     for ( i = 0; i < 20; i++) {
     console.log(i)     
         var month = {
             "month": "May 2015",

             "moneyStreams": [{
                 "streamId": "1",
                 "title": "End of month balance",
                 "returning": false,
                 "type": "eom",
                 "order": 10000,
                 "sizeX": 2,
                 "sizeY": 1,
                 "row": 0,
                 "col": 0,
                 "accounts": {
                     "cash": {
                         "balance": "$32,000.00",
                         "streamChange": "$4,000.00"
                     },
                     "savings": {
                         "balance": "$7,000.00",
                         "streamChange": "($13,000.00)"
                     },
                     "debt": {
                         "balance": "$100.00"
                     }
                 }
             }]

         };
         console.log(month)
         months[i] = month;

     }
     return months;
 }

 var finstreamz = angular.module('finstreamz', ['gridster']);
 finstreamz.config(function($interpolateProvider) {
     $interpolateProvider.startSymbol('{[').endSymbol(']}');
 });
 finstreamz.controller('StreamsTableCtrl', ['$rootScope', '$scope', '$http',
     function($rootScope, $scope, $http) {
         $http.get('/testMonthlyStreams.json').success(function(data) {
             $scope.monthlyStatements = data;
             $scope.monthlyStatements.months = randomFutureMonths().concat($scope.monthlyStatements.months.concat());
         });




         $scope.gridsterOpts = {
             columns: 1,
             colWidth: 'auto',
             rowHeight: 70,
             margins: [0, 0],
             outerMargin: false,
             pushing: true,
             floating: true,
             draggable: {
                 enabled: true
             },
             resizable: {
                 enabled: false,
                 handles: ['n', 'e', 's', 'w', 'se', 'sw']
             }
         };
         $scope.selectStream = function(streamId) {
             $rootScope.$broadcast('streamSelected', streamId);
             $scope.selectedStreamId = streamId;
         };
     }
 ]);

 finstreamz.controller('StreamDefinitionCtrl', ['$rootScope', '$scope', '$http',
     function($rootScope, $scope, $http) {
         $rootScope.$on('streamSelected', function(event, streamId) {
             $scope.streamId = streamId;
             $scope.updateStreamDefinition();
         });

         $scope.updateStreamDefinition = function() {
             $http.get('/testStreamDefinitions' + $scope.streamId + '.json').success(function(data) {
                 $scope.selectedStreamDefinition = data;
             })

         }

     }
 ]);
