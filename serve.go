package main

import (
	"encoding/json"
	"encoding/csv"
	"fmt"
	"github.com/balopat/finstreamz/budget"
	"github.com/go-martini/martini"
	"github.com/martini-contrib/render"
	"os"
	"time"
	"net/http"
)

const shortForm = "2006-01-02"
var b, b2 budget.Budget


func main() {
	m := martini.Classic()
	m.Use(render.Renderer(
		render.Options{Layout: "layout"}))
	
	m.Get("/statement/:date", SatementService)
	m.Get("/statements/:from/:to", SatementSeriesService)
	m.Get("/csv/statements/:from/:to", SatementSeriesCSVService)
	m.Get("/", Application)
	m.Run()
}

func Application(params martini.Params, r render.Render) {
	r.HTML(200, "application", "")
}

func SatementSeriesCSVService(params martini.Params, res http.ResponseWriter, req *http.Request) {
		res.WriteHeader(200)

		dateFrom := params["from"]
		dateTo := params["to"]

		from, errFrom := time.Parse(shortForm, dateFrom )
		if errFrom != nil {
			fmt.Println("error: ", errFrom)
		} 

		to, errTo := time.Parse(shortForm, dateTo)
		if errTo != nil {
			fmt.Println("error: ", errTo)
		} 

		statements := b.GetStatementsBetween(from, to)

		w := csv.NewWriter(res)

		var accounts []string
		for account := range statements[0].Balances {
			accounts = append(accounts, account)
		}
		columns := append([]string{"date"}, accounts...)
		w.Write(columns)

		for s := range statements {
			statement := statements[s]
			rowValues := []string{ fmt.Sprintf("%s",statement.Date) }
			for a := range accounts {
				account := accounts[a]
				balance := statement.Balances[account].Notional
				rowValues = append(rowValues, fmt.Sprintf("%.2f",balance))
			}
			w.Write(rowValues)
		}
		w.Flush()
	}

func SatementSeriesService(params martini.Params, r render.Render) {
		dateFrom := params["from"]
		dateTo := params["to"]

		from, errFrom := time.Parse(shortForm, dateFrom )
		if errFrom != nil {
			fmt.Println("error: ", errFrom)
		} 

		to, errTo := time.Parse(shortForm, dateTo)
		if errTo != nil {
			fmt.Println("error: ", errTo)
		} 

		statements := b.GetStatementsBetween(from, to)
		
			
		j, err := json.MarshalIndent(statements, "", "\t")
		if err != nil {
			fmt.Println("error: ", err)
		} else {
			os.Stdout.Write(j)
			r.JSON(200, statements)
		}
		
	}

 func SatementService(params martini.Params, r render.Render) {
		t, err := time.Parse(shortForm, params["date"])
		if err != nil {
			fmt.Println("error: ", err)
		} else {
			Statement := b.GetStatement(t)
			j, err := json.MarshalIndent(Statement, "", "\t")
			if err != nil {
				fmt.Println("error: ", err)
			} else {
				os.Stdout.Write(j)
				r.JSON(200, Statement)
			}
		}
	}
